#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <fstream>

//#define LINUX

#ifdef LINUX

	#include <termios.h>
	#include <unistd.h>
	#define ClearScreen() system("clear");
	int getch()
	{
    	struct termios oldattr, newattr;
    	int ch;
    	tcgetattr( STDIN_FILENO, &oldattr );
    	newattr = oldattr;
    	newattr.c_lflag &= ~( ICANON | ECHO );
   		tcsetattr( STDIN_FILENO, TCSANOW, &newattr );
    	ch = getchar();
    	tcsetattr( STDIN_FILENO, TCSANOW, &oldattr );
    	return ch;
	}

#elif !LINUX

	#include <conio.h>
	#define ClearScreen(); system("cls");
	
#endif

#define ROZMIARPLANSZY 10
#define ILOSCSTATKOW 9
//#define getch() 

class Punkt
{
		int x, y;
	public:
		Punkt() : x(0), y(0) {}
		
		Punkt(int x, int y) : x(x), y(y) {}
		
		int getX() { return x; }
	
		int getY() { return y; }
};

bool sprawdzPunkt(int x, int y)
{
	if (x < 0 || x > ROZMIARPLANSZY-1 || y < 0 || y > ROZMIARPLANSZY-1)
				return false;
			return true;
}

class Statek;

class Maszt
{
		bool trafiony;
		Statek* statek;
	public:
		Maszt() : trafiony(false), statek(NULL) {}
		
		void traf() {trafiony = true;}
		
		bool czyTrafiony() {return trafiony;}
		
		bool czyNalezy() {if (statek != NULL) return true; return false;}
		
		void przypisz(Statek* statek) {this->statek = statek;}
		
		Statek* wlasciciel() {return statek;}
		
		std::string toString(bool pokazStatek)
		{
            if (czyTrafiony())
            {
                if (czyNalezy())
                    return "X";
            	else
                    return "+";
            }
            else
            {
                if (czyNalezy())
				{
                    if (pokazStatek)
                        return "#";
                    else
                        return "_";
                }
				else
				{
                    return "_";
				}
            }
        }
};

class Gra
{
		Maszt plansza[2][ROZMIARPLANSZY][ROZMIARPLANSZY];
	public:
		
		typedef Maszt (*pointer_to_arrays)[ROZMIARPLANSZY];
		
		pointer_to_arrays getPlansza1() {return plansza[0];}
		pointer_to_arrays getPlansza2() {return plansza[1];}
		
		
		void rysuj(Maszt plansza[][ROZMIARPLANSZY],bool pokazStatki)
		{
			std::cout << "  ";
			for (int i = 0; i < ROZMIARPLANSZY; i++)
					std::cout << i << " ";
			std::cout << std::endl;
			for (int i = 0; i < ROZMIARPLANSZY; i++)
			{
				std::cout << (char)(i+65) << "|";
				for (int j = 0; j < ROZMIARPLANSZY; j++)
					std::cout << plansza[i][j].toString(pokazStatki) << "|";
				std::cout << std::endl;
			}
		}
};

class Statek
{
	int sprawdzMiejsce(int x, int y)
	{
		for (int i = x-1; i <= x+1; i++)
			for (int j = y-1; j <= y+2; j++)			
				if (sprawdzPunkt(i,j))
					if (plansza[i][j].wlasciciel() != NULL && plansza[i][j].wlasciciel() != this )
						return 1;
		return 0;
	}
	
	protected:
		bool zatopiony;
		typedef Maszt (*pointer_to_arrays)[ROZMIARPLANSZY];
		pointer_to_arrays plansza;
		Maszt** maszty;
		int rozmiar;
		std::string nazwa;
		
	public:
		Statek(pointer_to_arrays plansza, int rozmiar, std::string nazwa) : zatopiony(false), plansza(plansza), rozmiar(rozmiar), nazwa(nazwa), maszty(new Maszt*[rozmiar]) {}
		
		int ustaw(Punkt p, int kierunek)
		{
			int j = 0;
			switch(kierunek)
			{
				case 1:
					//for (int i = p.getX(); i > p.getX()-podajRozmiar(); i--)
					for (int i = 0; i > -podajRozmiar(); i--)
					{
						if (!sprawdzPunkt(i+p.getX(),p.getY()))
							return 1;
					}
					for (int i = p.getX(); i > p.getX()-podajRozmiar(); i--)
					{
						if (sprawdzMiejsce(i, p.getY() ) == 1)
							return 1;
					}
					for (int i = p.getX(); i > p.getX()-podajRozmiar(); i--)
					{
						maszty[j] = &plansza[i][p.getY()];
						maszty[j]->przypisz(this);
						j++;
					}
					return 0;

				case 2:
					for (int i = p.getX(); i < p.getX()+podajRozmiar(); i++)
					{
						if (!sprawdzPunkt(i,p.getY()))
							return 1;
					}
					for (int i = p.getX(); i < p.getX()+podajRozmiar(); i++)
					{
						if (sprawdzMiejsce(i, p.getY() ) == 1)
							return 1;
					}
					for (int i = p.getX(); i < p.getX()+podajRozmiar(); i++)
					{
						maszty[j] = &plansza[i][p.getY()];
						maszty[j]->przypisz(this);
						j++;
					}
					return 0;
					
				case 3:
					for (int i = p.getY(); i > p.getY()-podajRozmiar(); i--)
					{
						if (!sprawdzPunkt(p.getX(),i))
							return 1;
					}
					for (int i = p.getY(); i > p.getY()-podajRozmiar(); i--)
					{
						if (sprawdzMiejsce(p.getX(),i) == 1)
							return 1;
					}
					for (int i = p.getY(); i > p.getY()-podajRozmiar(); i--)
					{
						maszty[j] = &plansza[p.getX()][i];
						maszty[j]->przypisz(this);
						j++;
					}
					return 0;
					
				case 4:
					for (int i = p.getY(); i < p.getY()+podajRozmiar(); i++)
					{
						if (!sprawdzPunkt(i,p.getY()))
							return 1;
					}
					for (int i = p.getY(); i < p.getY()+podajRozmiar(); i++)
					{
						if (sprawdzMiejsce(p.getX(),i) == 1)
							return 1;
					}
					for (int i = p.getY(); i < p.getY()+podajRozmiar(); i++)
					{
						maszty[j] = &plansza[p.getX()][i];
						maszty[j]->przypisz(this);
						j++;
					}
					return 0;
					
				default:
					return 1;
			}
		}
		
		bool czyZatopiony() 
		{
			for (int i = 0; i < podajRozmiar(); i++)
			{
				if(!maszty[i]->czyTrafiony())
					return false;
			}
			return true;
		}
		
		int podajRozmiar() { return rozmiar; }
		
		std::string podajNazwe() { return nazwa; }
};

class Statek4Masztowy : public Statek
{
	public:
		Statek4Masztowy(pointer_to_arrays plansza) : Statek(plansza, 4, "Statek czteromasztowy") { }
};

class Statek3Masztowy : public Statek
{
	public:
		Statek3Masztowy(pointer_to_arrays plansza) : Statek(plansza, 3, "Statek trzymasztowy") { }
};

class Statek2Masztowy : public Statek
{
	public:
		Statek2Masztowy(pointer_to_arrays plansza) : Statek(plansza, 2, "Statek dwumasztowy") { }
};

class Statek1Masztowy : public Statek
{
	public:
		Statek1Masztowy(pointer_to_arrays plansza) : Statek(plansza, 1, "Statek jednomasztowy") { }
};

class Gracz
{
	protected:
		int id;
		typedef Maszt (*pointer_to_arrays)[ROZMIARPLANSZY];
		pointer_to_arrays mojaPlansza;
		pointer_to_arrays nieMojaPlansza;
		Statek* statki[ILOSCSTATKOW];
	public:
		Gracz(Gra* gra, int id) : id(id) 
		{
			if (id == 0)
			{
				mojaPlansza = gra->getPlansza1();
				nieMojaPlansza = gra->getPlansza2();
			}
			if (id == 1)
			{
				mojaPlansza = gra->getPlansza2();
				nieMojaPlansza = gra->getPlansza1();
			}
			statki[0] = new Statek4Masztowy(mojaPlansza);
			statki[1] = new Statek3Masztowy(mojaPlansza);
			statki[2] = new Statek3Masztowy(mojaPlansza);
			statki[3] = new Statek2Masztowy(mojaPlansza);
			statki[4] = new Statek2Masztowy(mojaPlansza);
			statki[5] = new Statek2Masztowy(mojaPlansza);
			statki[6] = new Statek1Masztowy(mojaPlansza);
			statki[7] = new Statek1Masztowy(mojaPlansza);
			statki[8] = new Statek1Masztowy(mojaPlansza);
		}
		
		~Gracz()
		{
			for (int i = 0; i < ILOSCSTATKOW; i++)
			{
				delete statki[i];
			}
		}
		
		int traf(Punkt p)
		{
			nieMojaPlansza[p.getX()][p.getY()].traf();
			if (nieMojaPlansza[p.getX()][p.getY()].czyNalezy())
			{
				if (nieMojaPlansza[p.getX()][p.getY()].wlasciciel()->czyZatopiony())
					return 2;
				return 1;
			}
			return 0;
		}
		
		virtual Punkt podajXY() = 0;
		virtual int podajKierunek() = 0;
		
		int geId() {return id;}
		
		bool przegral()
		{
			for (int i = 0; i < ILOSCSTATKOW; i++)
			{
				if (!statki[i]->czyZatopiony())
					return false;
			}
			return true;
		}
		
		void ustawStatek(int numer)
		{
			Punkt p;
			int kierunek, wynik = 2;
			do
			{
					if (wynik == 1)
						std::cout << "Nie mozesz ustawic statku w tym miejscu!" << std::endl;
					p = podajXY();
					if (statki[numer]->podajRozmiar() == 1)
						kierunek = 1;
					else
						kierunek = podajKierunek();
			}while(wynik = statki[numer]->ustaw(p,kierunek) != 0);
		}
		
		Statek* getStatek(int numer) { return statki[numer]; }
};

class Czlowiek : public Gracz
{
	public:
		Czlowiek(Gra* gra, int id) : Gracz(gra,id) {}
		
		Punkt podajXY() 
		{
			std::string pole;
			std::cout << "Podaj pole:" << std::endl;
			do
			{
                do
                {
                    std::cin >> pole;
                    pole[0] = toupper(pole[0]);
                    if (pole.length() != 2)
                    {
                        std::cout << "Podaj 2 znaki!" << std::endl;
                    }
                }while(pole.length() != 2);
                                
                if (!isalpha(pole[0]) || !isdigit(pole[1]) )
                {
                    std::cout << "Podaj litere i cyfre!" << std::endl;
                        continue;
                }
				
				if ((int)pole[0] < 65 || (int)pole[1] > 65+ROZMIARPLANSZY-1)
					std::cout << "Podaj litere od A do J!" << std::endl;
				if ((int)pole[1] < 48 || (int)pole[1] > 48+ROZMIARPLANSZY-1)
					std::cout << "Podaj cyfre od 0 do 9!" << std::endl;
				if (nieMojaPlansza[(int)pole[0]-65][(int)pole[1]-48].czyTrafiony())
					std::cout << "Juz wybrales to pole. Wybierz inne!" << std::endl;
			}while( ( (int)pole[0] < 65 || (int)pole[1] > 65+ROZMIARPLANSZY-1 ) || ((int)pole[1] < 48 || (int)pole[1] > 48+ROZMIARPLANSZY-1) || (nieMojaPlansza[(int)pole[0]-65][(int)pole[1]-48].czyTrafiony()) );
			Punkt p((int)pole[0]-65,(int)pole[1]-48);
			return p;
		}
		
		int podajKierunek()
		{
			std::cout << "Podaj kierunek" << std::endl << "1.Gora" << std::endl << "2.Dol" << std::endl << "3.Lewo" << std::endl << "4.Prawo" << std::endl;
			int a;
			do
			{
				std::cin >> a;
				if (a < 1 || a > 4)
					std::cout << "Podaj liczbe z zakresu od 1 do 4!" << std::endl;
			}while(a < 1 || a > 4);
			return a;
		}
};

class Komputer : public Gracz
{
	public:
		Komputer(Gra* gra, int id) : Gracz(gra,id) { srand (time(NULL)); }
		
		Punkt podajXY() {
			int x, y;
			do
			{
					x = rand() % ROZMIARPLANSZY;
					y = rand() % ROZMIARPLANSZY;
			}while(nieMojaPlansza[x][y].czyTrafiony());
			std::cout << (char)(x+65) << y << std::endl;
			Punkt p(x,y);
			return p;
		}
		
		int podajKierunek() { return rand() % 4+1; }
};

class Log
{
		std::fstream log;
	public:
		Log() {
			log.open( "log.txt", std::ios::out );
		}
		void zapisz(std::string s)
		{
			log << s << std::endl;
		}
	
};

void wybierzGracza(Gra* gra, Gracz* gracz[], int id )
{
	std::cout << "Gracz " << id+1 << " to?" << std::endl << "1. Czlowiek" << std::endl << "2. Komputer" << std::endl;
	int a;
	do
	{
		std::cin >> a;
		if (a < 1 || a > 2)
			std::cout << "Podaj liczbe z zakresu od 1 do 2!"<< std::endl;
		if (a == 1)
			gracz[id] = new Czlowiek(gra,id);
		if (a == 2)
			gracz[id] = new Komputer(gra,id);
	}while(a < 1 || a > 2);
}

void ustawStatki(Gra* gra, Gracz* gracz)
{
	ClearScreen();
	std::cout << "Kolej gracza " << gracz->geId()+1 << std::endl;
	getch();
	ClearScreen();
	for (int i = 0; i < ILOSCSTATKOW; i++)
	{
		ClearScreen();
		std::cout << "Gracz " << gracz->geId()+1 << ":" << std::endl << "----------------------" << std::endl;
		if (gracz->geId() == 0)
			gra->rysuj(gra->getPlansza1(),1);
		else
			gra->rysuj(gra->getPlansza2(),1);
		std::cout << "Ustaw swoj " << gracz->getStatek(i)->podajNazwe() << std::endl;
		gracz->ustawStatek(i);
		ClearScreen();
		std::cout << "Gracz " << gracz->geId()+1 << ":" << std::endl << "----------------------" << std::endl;
		if (gracz->geId() == 0)
			gra->rysuj(gra->getPlansza1(),1);
		else
			gra->rysuj(gra->getPlansza2(),1);
	}
	ClearScreen();
}

void rysuj(Gra* gra, Gracz* gracz)
{
	ClearScreen();
	std::cout << "Gracz " << gracz->geId()+1 << ":" << std::endl;
	std::cout << "----------------------" << std::endl;
	if (gracz->geId() == 0)
		gra->rysuj(gra->getPlansza2(),2);
	else
		gra->rysuj(gra->getPlansza1(),2);
	std::cout << "----------------------" << std::endl;
	if (gracz->geId() == 0)
		gra->rysuj(gra->getPlansza1(),1);
	else
		gra->rysuj(gra->getPlansza2(),1);
	std::cout << "----------------------" << std::endl;
}

bool graj(Gra* gra, Gracz* gracz, Gracz* przeciwnik)
{
	bool trafiony;
	do
	{
		trafiony = false;
		rysuj(gra,gracz);
		Punkt p = gracz->podajXY();
		if(int a = gracz->traf(p))
		{
			rysuj(gra,gracz);
			std::cout << (char)(p.getX()+65) << p.getY() << std::endl;
			std::cout << "Trafiony! ";
			if (a == 2) std::cout << "Zatopiony!";
			std::cout << std::endl;
			trafiony = true;
			getch();
		}
		else
		{
			rysuj(gra,gracz);
			std::cout << (char)(p.getX()+65) << p.getY() << std::endl;
			std::cout << "Pudlo!" << std::endl;
			getch();
		}
		
	}while(trafiony && !przeciwnik->przegral());
		
	if(przeciwnik->przegral())
	{
		std::cout << "Gracz "<< przeciwnik->geId()+1 << " przegral!" << std::endl;
		return true;
	}
	ClearScreen();
	std::cout << "Kolej gracza "<< przeciwnik->geId()+1 << std::endl;
	getch();
	ClearScreen();
	return false;
}

int main(int argc, char** argv) {
	ClearScreen();
	Gra gra;
	Log log;
	Gracz* gracz[2];
	
	wybierzGracza(&gra, gracz, 0);
	wybierzGracza(&gra, gracz, 1);
	
	ustawStatki(&gra, gracz[0]);
	ustawStatki(&gra, gracz[1]);
	
	while(true)
	{
		if( graj(&gra, gracz[0], gracz[1]) )
			break;
		if( graj(&gra, gracz[1], gracz[0]) )
			break;
	}
	std::cout << "Wcisnij ESC zeby wyjsc" << std::endl;
	char c;
	do
	{
		c = getch();
	}while( c != (int)27 );
	return 0;
}
